<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  
  <head>
      <style>
      table, th, td {
          border: 1px solid black;
      }
      </style>
  </head>
  
  <body>
    
    <form id="sqlForm" name="sqlForm" action="sqlServlet" method="POST">
        <textarea id="sqlForm-sql" name="sql" rows="8" cols="100"><c:out value="${sql}" escapeXml="false" /></textarea>
        <br><br>
        <input type="submit" value="submit" />
    <form>    
    
    <br><br>
    
    <table>
        <c:out value="${headerTitles}" escapeXml="false" />
        <c:out value="${rows}" escapeXml="false" />
    </table>
    
    <br><br>
    total Rows: <c:out value="${rowCnt}" escapeXml="false" />
    
    <br><br>
    
    <c:out value="${sqlMessage}" escapeXml="false" />
    
  </body>

</html>