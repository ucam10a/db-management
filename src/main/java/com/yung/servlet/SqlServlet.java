package com.yung.servlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -3134113725294025890L;
    
    private static final Logger logger = LoggerFactory.getLogger(SqlServlet.class);
    
    private static final ClobConverter cConverter = new ClobConverter();
    private static final NumberConverter nConverter = new NumberConverter();
    private static final TimeConverter tConverter = new TimeConverter();
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        
        String sql = req.getParameter("sql");
        if (sql == null) {
            sql = "";
        }
        req.setAttribute("sql", sql);
        logger.info("sql: " + sql);
        
        int rowCnt = 0;
        boolean moreRecord = false;
        StringBuilder headerSb = new StringBuilder();
        StringBuilder rowsSb = new StringBuilder();
        if ("".equals(sql)) {
            headerSb.append("<tr><td></td></tr>");
            rowsSb.append("<tr><td></td></tr>");
        } else {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection conn = getConnection();
            try {
                ps = conn.prepareStatement(sql);
                String lowerSql = sql.toLowerCase();
                lowerSql = lowerSql.trim();
                if (lowerSql.startsWith("select")) {
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        if (rowCnt > 1000) {
                            moreRecord = true;
                            break;
                        }
                        ResultSetMetaData metaData = rs.getMetaData();
                        int cnt = metaData.getColumnCount();
                        if (headerSb.toString().equals("")) {
                            if (cnt > 0) {
                                headerSb.append("<tr>");
                            }
                            for (int i = 1; i <= cnt; i++) {
                                String columnName = metaData.getColumnName(i);
                                headerSb.append("<td>");
                                headerSb.append(columnName);
                                headerSb.append("</td>");
                            }
                            if (cnt > 0) {
                                headerSb.append("</tr>");
                            }
                        }
                        if (cnt > 0) {
                            rowsSb.append("<tr>");
                        }
                        for (int i = 1; i <= cnt; i++) {
                            Object val = rs.getObject(i);
                            String content = convertVal(val);
                            rowsSb.append("<td>");
                            rowsSb.append(content);
                            rowsSb.append("</td>");
                        }
                        if (cnt > 0) {
                            rowsSb.append("</tr>");
                        }
                        rowCnt++;
                    }
                    if (headerSb.toString().equals("")) {
                        headerSb.append("<tr><td></td></tr>");
                        rowsSb.append("<tr><td></td></tr>");
                    }
                } else {
                    ps.executeUpdate();
                }
                req.setAttribute("headerTitles", headerSb.toString());
                req.setAttribute("rows", rowsSb.toString());
                req.setAttribute("rowCnt", rowCnt + "");
                if (moreRecord) {
                    req.setAttribute("sqlMessage", "success! but only show first 1000 records!");
                } else {
                    req.setAttribute("sqlMessage", "success!");
                }
                
                
            } catch (Exception e) {
                logger.error(e.toString(), e);
                throw new ServletException(e);
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    throw new ServletException(e);
                }
            }
        }
        
        RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        dispatcher.forward(req, resp);
        
    }

    private static String convertVal(Object val) throws Exception {
        if (val == null) {
            return "";
        }
        if (val instanceof String) {
            return val.toString();
        }
        if (val instanceof Clob) {
            String content = (String) cConverter.Convert(val, String.class);
            return content;
        }
        if (nConverter.isNumber(val.getClass())) {
            BigDecimal num = (BigDecimal) nConverter.Convert(val, BigDecimal.class);
            return num.toPlainString();
        }
        if (tConverter.isTime(val.getClass())) {
            Date dt = (Date) tConverter.Convert(val, Date.class);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(dt);
        }
        return "";
    }

    private Connection getConnection() {
        //String driver = "org.hsqldb.jdbcDriver";
        //String user = "sa";
        //String password = "";
        //String url = "jdbc:hsqldb:hsql://localhost/ikos";
        //return getConnection(driver, user, password, url);
        return getConnection("java/ds/testDS");
    }
    
    public static Connection getConnection(String jndi) {
        try {
            InitialContext ctx = new InitialContext();
            return  ((DataSource) ctx.lookup(jndi)).getConnection();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection(String driver, String user, String password, String url) {
        try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                logger.error(e.toString(), e);
                throw new RuntimeException(e);
            }
            Connection conn = DriverManager.getConnection(url, user, password);
            return conn;
        } catch (SQLException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
}
